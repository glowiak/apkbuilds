# apkbuilds

My repository of APKBUILDs for Alpine Linux

# Building packages from source

Clone this git repo, and use abuild to build APK packages

# Binaries

First grab the signatures:

	mkdir -p /etc/apk/keys && cd /etc/apk/keys && wget http://codeberg.org/glowiak/apkbuilds/raw/branch/master/glowiak1111@yandex.com-61338572.rsa.pub

Then add following line to /etc/apk/repositories:

	http://codeberg.org/glowiak/apkbuilds/raw/branch/master/package-repository/apkbuilds

At the end update repositories with 'apk update'
